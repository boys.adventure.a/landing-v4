/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors:{
      transparent: 'transparent',
      BA_white: '#F8FAFC',
      BA_blue: '#36C2F7',
      BA_grey: '#333F4D',
      BA_text_grey: '#959CB3',
    },
    extend: {
      fontFamily: {
        bahnschrift: ["Bahnschrift", "sans-serif"],
        boldstrom: ["Boldstrom", "sans-serif"],
      },
    },
    screens: {
      'mobile': '360px',
      'lg': '1024px',
      'xl': '1280px',
    },
  },
  plugins: [],
}