import './App.css';
import React from 'react';


import Fullpage from './components/Fullpage';

function App() {
  return (
    <div className="App">
      <Fullpage/>
    </div>
  );
}

export default App;
