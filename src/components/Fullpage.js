import React from 'react';
import ReactFullpage from '@fullpage/react-fullpage';
import { motion } from 'framer-motion';

import '../overrides.css'

import NumberCounter from './NumberCounter';
import HeaderMenu from './HeaderMenu';
import Aptos from './Aptos';
import Slider from './Slider';

export default function Fullpage() {
    return (
        <ReactFullpage
        licenseKey = {'P7NKK-UER8J-IMF06-U9ON9-LOOEL'}
        scrollingSpeed = {950}
        navigation
        navigationPosition='left'
        render={({ state, fullpageApi }) => {
            return (
            <ReactFullpage.Wrapper> 
                <div className="section">
                    <section id="top" className="snap-start h-screen w-screen z-20 overflow-x-hidden">
                        <div  className="flex h-screen bg-white z-20">
                            <div className="xl:w-1/4 lg:w-1/4 mobile:w-[33%] flex flex-col items-center justify-center gap-12 z-20 xl:pl-0 lg:pl-0 mobile:pl-4" id="bg_white">
                                <HeaderMenu/>
                            </div>
                            <div className="xl:w-3/4 lg:w-3/4 mobile:w-[67%] bg-[url('https://i.ibb.co/gTVn1K5/background-top.jpg')] bg-cover bg-no-repeat flex flex-col justify-center xl:bg-left lg:bg-center mobile:bg-center">
                                <div className="xl:ml-12">
                                    <motion.div initial={{opacity: 0}} animate={{opacity: 1}} transition={{type: 'spring', duration: 1.5, delay: 0.5}}>
                                        <h1 className="text-center xl:text-8xl lg:text-7xl mobile:text-4xl text-BA_white font-boldstrom animation">BOYS <br></br> ADVENTURE</h1>
                                    </motion.div>
                                    <></>  
                                    <motion.div initial={{opacity: 0}} animate={{opacity: 1}} transition={{type: 'spring', duration: 1.5, delay: 1}}>
                                        <h2 className="text-center xl:text-2xl lg:text-xl mobile:text-lg text-BA_blue font-bold xl:tracking-[12px] font-bahnschrift">aptos ecosystem</h2>
                                    </motion.div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                </div>
                <div className="section">
                <section id="explore" className="snap-start xl:h-screen lg:h-screen xl:w-screen lg:w-screen mobile:h-full">
                    <div className="flex lg:py-0 xl:py-0 h-screen">
                        <div className="w-1/4 h-[20vh] linear_gradient z-10"><></>
                        </div>
                        <div className="w-3/4 xl:flex xl:flex-row lg:flex lg:flex-row mobile:flex-col justify-between content-center mobile:pt-16 xl:pt-0 lg:pt-0">
                            <div className="xl:w-[30%] lg:w-[30%] mobile:w-full flex flex-col justify-start xl:pt-[10%]">
                                <motion.div initial={{y: '5vw'}} animate={{y: '0vw'}} transition={{type: 'spring', duration: 1.5, delay: 3}}>
                                    <h2 className="xl:text-left lg:text-left mobile:text-center mobile:mb-4 xl:text-5xl lg:text-4xl mobile:text-3xl xl:font-medium lg:font-normal xl:pb-11 lg:pb-7 font-boldstrom">EXPLORE <br></br><span className="text-BA_blue">THE WORLD</span></h2>
                                    <p className="xl:text-justify lg:text-justify xl:pr-14 lg:px-0 mobile:px-8 mobile:text-justify mobile:mb-4 xl:text-xl text-BA_text_grey xl:font-medium xl:leading-7 font-bahnschrift">
                                        Boys Adventure is an NFT project built on the Aptos blockchain.
                                        Exploration, Creativity, Cooperation are the words that best define 
                                        what Boys Adventure is. <br></br>To bring people together with our art, we are 
                                        creating a universe that reminds us of our childhood: 
                                        Exploration, Dreaming, Adventure. To bring the communities together, 
                                        we are and will be setting up artistic collaborations in different forms, 
                                        bringing value to each other’s projects will allow the Aptos ecosystem to grow in the right direction.
                                    </p>
                                </motion.div>
                            </div>
                            <div className="xl:w-max lg:w-max mobile:w-full flex flex-row justify-end mobile:pb-16 xl:pb-0 lg:pb-0">
                                <img src="/assets/explore.png" alt="Explore the world with Boys Adventure" className="h-[100%] w-max my-auto"/>
                            </div>
                        </div>
                    </div>
                </section>
                </div>
                <div className="section">
                    <section id="slider" className="snap-start h-screen xl:w-screen lg:w-screen mobile:w-[85%] xl:ml-0 lg:ml-0 mobile:ml-auto">
                        <div  className="xl:h-1/4 lg:h-1/4 mobile:h-[10%] flex flex-col justify-center xl:pt-0 lg:pt-0 mobile:pt-24">
                            <motion.div
                                initial={{y: '5vw', opacity: 0, scale: 0.8}}
                                animate={{y: '0vw', opacity: 1, scale: 1}}
                                transition={{type: 'spring', duration: 1.5, delay: 5}}
                            >
                                <h2 className="text-center xl:font-medium lg:font-normal xl:text-3xl lg:text-3xl mobile:text-2xl tracking-normal font-boldstrom">
                                    <span className="text-BA_blue">UNIQUE</span> NOT JUST <span className="text-BA_blue">FOR YOU</span> BUT FOR THE <span className="text-BA_blue custom_cursor"><a href="https://twitter.com/BoysAdvnture" target="_blank" rel="noreferrer noopener" className='custom_cursor'>WORLD</a></span>
                                </h2>
                            </motion.div>
                        </div>
                        <div className="h-2/4 w-screen xl:flex xl:flex-row lg:flex lg:flex-row mobile:flex mobile:flex-col mobile:justify-around">
                            <div className="xl:w-1/4 lg:w-1/4 mobile:w-0"></div>
                            <Slider/>
                            <div className="xl:w-1/4 lg:w-1/4 mobile:w-max xl:flex xl:flex-col lg:flex lg:flex-col mobile:flex mobile:flex-row items-center justify-center xl:ml-0 lg:ml-0 mobile:ml-[20%] xl:gap-6 lg:gap-6 mobile:gap-2">
                                <NumberCounter/>
                            </div>
                        </div>
                        <div className="xl:h-1/4 lg:h-1/4 mobile:h-[20%] xl:w-[30%] lg:w-[55%] xl:m-auto lg:m-auto mobile:mt-12">
                            <motion.div
                                    initial={{opacity: 0, scale: 0.8}}
                                    animate={{opacity: 1, scale: 1}}
                                    transition={{type: 'spring', duration: 1.5, delay: 5}}
                                >
                                <p className="text-center text-lg text-BA_text_grey xl:font-medium lg:font-normal xl:leading-7 lg:leading-5 font-bahnschrift xl:px-0 lg:px-0 mobile:px-6">
                                    Think of Boys Adventure as a laboratory, 
                                    we will do a lot of experiments, some ideas 
                                    will be good and some less and we will learn 
                                    from our mistakes. You too will help us to experiment, 
                                    you will become a mad scientist. Boys Adventure, a group of mad scientists.
                                </p>
                            </motion.div>
                        </div>
                    </section>
                </div>
                <div className="section">
                    <Aptos/>
                </div>
            </ReactFullpage.Wrapper>
            );
        }}
        />
    )
}
