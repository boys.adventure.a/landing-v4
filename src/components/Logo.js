import React from 'react';
import { motion } from 'framer-motion';

/*
{pressed === false ? (
    pressedOnce === false ? 
        <motion.div initial={{opacity: 0, scale: 0.9}} animate={{opacity: 1, scale: 1.3}} transition={{type: 'spring', duration: 1.5, delay: 9.5}}>
            <div className='w-full h-full'>
                <img src={'/assets/Aptos_Logo.png'} alt="Aptos logo" className="h-full "/>
            </div>
        </motion.div>
        : 
        <motion.div initial={{opacity: 0, scale: 0.9}} animate={{opacity: 1, scale: 1.3}} transition={{type: 'spring', duration: 1.5, delay: 0.5}}>
            <div className='w-full h-full'>
                <img src={'/assets/Aptos_Logo.png'} alt="Aptos logo" className="h-full "/>
            </div>
        </motion.div> 
) : 
    <div className='w-full h-full'>
        <img src={'/assets/Logo_pp.png'} alt="BoysAdventure logo" className="h-full rounded-full" />
    </div>
}
const [pressed, setPressed] = useState(false);
    const [pressedOnce, setPressedOnce] = useState(false);

    function press() {
        if (pressed === false) {
            setPressed(true);
        }
        else {
            setPressed(false);
        }
    }
    function pressOnce() {
        setPressedOnce(true)
    }
 onClick={() => {press();pressOnce()}}
 , { useState }
*/

export default function Logo() {
    return (
        <button className="w-full h-full mt-4 opacity-100 transition ease-in-out delay-[50ms] hover:scale-110 duration-300">
            <motion.div initial={{opacity: 0, scale: 0.9}} animate={{opacity: 1, scale: 1.3}} transition={{type: 'spring', duration: 1.5, delay: 8.5}}>
                <a href="https://twitter.com/BoysAdvnture" target="_blank" rel="noreferrer noopener">
                    <div className='w-full h-full'>
                        <img src={'/assets/Aptos_Logo.png'} alt="Aptos logo" className="h-full "/>
                    </div>
                </a>
            </motion.div>
        </button>
    )
}
