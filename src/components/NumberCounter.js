import React from 'react'
import CountUp from 'react-countup';

export default function NumberCounter() {
  return (
    <>
        <span className="w-full text-BA_blue text-center xl:font-medium lg:font-normal xl:text-6xl lg:text-5xl mobile:text-3xl font-boldstrom">  
            <CountUp end={5} delay={4} duration={1}/>
        </span>
        <span className="w-full text-BA_blue text-center xl:font-medium lg:font-normal xl:text-6xl lg:text-5xl mobile:text-3xl font-boldstrom">
            <CountUp end={0} />
        </span>
        <span className="w-full text-BA_blue text-center xl:font-medium lg:font-normal xl:text-6xl lg:text-5xl mobile:text-3xl font-boldstrom">
            <CountUp end={0} />
        </span>
        <span className="w-full text-BA_blue text-center xl:font-medium lg:font-normal xl:text-6xl lg:text-5xl mobile:text-3xl font-boldstrom">
            <CountUp end={0} />
        </span>
        <span className="xl:h-20 lg:h-20 mobile:h-full w-full text-center text-BA_text_grey xl:font-medium lg:font-normal xl:text-5xl lg:text-4xl mobile:text-3xl font-boldstrom">NFT</span>
    </>
  )
}
