import React from 'react'

export default function HeaderMenu() {
//img className="xl:h-12 xl:w-12 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6" src="./assets/home_btn.svg" alt="button home"/>
/*
<>
  <div className="xl:h-32 xl:w-32 bg-BA_grey rounded-[33px] mobile:rounded-[20px] flex z-10 lg:h-24 lg:w-24 mobile:h-16 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
    <a href='https://twitter.com/BoysAdvnture' target="blank_" className='flex flex-row w-full'>
      <svg viewBox="0 0 20 20" aria-hidden="true" class="xl:h-12 xl:w-12 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white"><path d="M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0 0 20 3.92a8.19 8.19 0 0 1-2.357.646 4.118 4.118 0 0 0 1.804-2.27 8.224 8.224 0 0 1-2.605.996 4.107 4.107 0 0 0-6.993 3.743 11.65 11.65 0 0 1-8.457-4.287 4.106 4.106 0 0 0 1.27 5.477A4.073 4.073 0 0 1 .8 7.713v.052a4.105 4.105 0 0 0 3.292 4.022 4.095 4.095 0 0 1-1.853.07 4.108 4.108 0 0 0 3.834 2.85A8.233 8.233 0 0 1 0 16.407a11.615 11.615 0 0 0 6.29 1.84"></path></svg>
    </a>
  </div>
  <div className="xl:h-64 xl:w-32 bg-BA_blue rounded-[33px] mobile:rounded-[20px] flex flex-column z-10 lg:h-56 lg:w-24 mobile:h-48 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
    <a href="" target="blank_" className='flex flex-row justify-center w-full'>MINT</a>
    <a href="" target="blank_" className='flex flex-row justify-center w-full'>MINT</a>
  </div>
  <div className="xl:h-32 xl:w-32 bg-BA_grey rounded-[33px] mobile:rounded-[20px] flex z-10 lg:h-24 lg:w-24 mobile:h-16 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
    <a href='https://discord.com/invite/wfZ8xMkF2h' target="blank_" className='flex flex-row w-full'>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" className='xl:h-14 xl:w-14 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white'>
        <path d="M524.531,69.836a1.5,1.5,0,0,0-.764-.7A485.065,485.065,0,0,0,404.081,32.03a1.816,1.816,0,0,0-1.923.91,337.461,337.461,0,0,0-14.9,30.6,447.848,447.848,0,0,0-134.426,0,309.541,309.541,0,0,0-15.135-30.6,1.89,1.89,0,0,0-1.924-.91A483.689,483.689,0,0,0,116.085,69.137a1.712,1.712,0,0,0-.788.676C39.068,183.651,18.186,294.69,28.43,404.354a2.016,2.016,0,0,0,.765,1.375A487.666,487.666,0,0,0,176.02,479.918a1.9,1.9,0,0,0,2.063-.676A348.2,348.2,0,0,0,208.12,430.4a1.86,1.86,0,0,0-1.019-2.588,321.173,321.173,0,0,1-45.868-21.853,1.885,1.885,0,0,1-.185-3.126c3.082-2.309,6.166-4.711,9.109-7.137a1.819,1.819,0,0,1,1.9-.256c96.229,43.917,200.41,43.917,295.5,0a1.812,1.812,0,0,1,1.924.233c2.944,2.426,6.027,4.851,9.132,7.16a1.884,1.884,0,0,1-.162,3.126,301.407,301.407,0,0,1-45.89,21.83,1.875,1.875,0,0,0-1,2.611,391.055,391.055,0,0,0,30.014,48.815,1.864,1.864,0,0,0,2.063.7A486.048,486.048,0,0,0,610.7,405.729a1.882,1.882,0,0,0,.765-1.352C623.729,277.594,590.933,167.465,524.531,69.836ZM222.491,337.58c-28.972,0-52.844-26.587-52.844-59.239S193.056,219.1,222.491,219.1c29.665,0,53.306,26.82,52.843,59.239C275.334,310.993,251.924,337.58,222.491,337.58Zm195.38,0c-28.971,0-52.843-26.587-52.843-59.239S388.437,219.1,417.871,219.1c29.667,0,53.307,26.82,52.844,59.239C470.715,310.993,447.538,337.58,417.871,337.58Z"/>
      </svg>
    </a> 
  </div>
</>
*/

/*
<div className="xl:h-32 xl:w-32 bg-BA_grey rounded-[33px] mobile:rounded-[20px] flex z-10 lg:h-24 lg:w-24 mobile:h-16 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
  <a href='https://twitter.com/BoysAdvnture' target="blank_" className='flex flex-row w-full'>
    <svg viewBox="0 0 20 20" aria-hidden="true" class="xl:h-12 xl:w-12 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white"><path d="M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0 0 20 3.92a8.19 8.19 0 0 1-2.357.646 4.118 4.118 0 0 0 1.804-2.27 8.224 8.224 0 0 1-2.605.996 4.107 4.107 0 0 0-6.993 3.743 11.65 11.65 0 0 1-8.457-4.287 4.106 4.106 0 0 0 1.27 5.477A4.073 4.073 0 0 1 .8 7.713v.052a4.105 4.105 0 0 0 3.292 4.022 4.095 4.095 0 0 1-1.853.07 4.108 4.108 0 0 0 3.834 2.85A8.233 8.233 0 0 1 0 16.407a11.615 11.615 0 0 0 6.29 1.84"></path></svg>
  </a>
</div>
<div className="xl:h-64 xl:w-32 bg-BA_blue rounded-[33px] mobile:rounded-[20px] flex flex-row z-10 lg:h-56 lg:w-24 mobile:h-48 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
    <a href="https://discord.com/invite/wfZ8xMkF2h" target="blank_" className='flex flex-row justify-center w-full'>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" className='xl:h-14 xl:w-14 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white'>
        <path d="M524.531,69.836a1.5,1.5,0,0,0-.764-.7A485.065,485.065,0,0,0,404.081,32.03a1.816,1.816,0,0,0-1.923.91,337.461,337.461,0,0,0-14.9,30.6,447.848,447.848,0,0,0-134.426,0,309.541,309.541,0,0,0-15.135-30.6,1.89,1.89,0,0,0-1.924-.91A483.689,483.689,0,0,0,116.085,69.137a1.712,1.712,0,0,0-.788.676C39.068,183.651,18.186,294.69,28.43,404.354a2.016,2.016,0,0,0,.765,1.375A487.666,487.666,0,0,0,176.02,479.918a1.9,1.9,0,0,0,2.063-.676A348.2,348.2,0,0,0,208.12,430.4a1.86,1.86,0,0,0-1.019-2.588,321.173,321.173,0,0,1-45.868-21.853,1.885,1.885,0,0,1-.185-3.126c3.082-2.309,6.166-4.711,9.109-7.137a1.819,1.819,0,0,1,1.9-.256c96.229,43.917,200.41,43.917,295.5,0a1.812,1.812,0,0,1,1.924.233c2.944,2.426,6.027,4.851,9.132,7.16a1.884,1.884,0,0,1-.162,3.126,301.407,301.407,0,0,1-45.89,21.83,1.875,1.875,0,0,0-1,2.611,391.055,391.055,0,0,0,30.014,48.815,1.864,1.864,0,0,0,2.063.7A486.048,486.048,0,0,0,610.7,405.729a1.882,1.882,0,0,0,.765-1.352C623.729,277.594,590.933,167.465,524.531,69.836ZM222.491,337.58c-28.972,0-52.844-26.587-52.844-59.239S193.056,219.1,222.491,219.1c29.665,0,53.306,26.82,52.843,59.239C275.334,310.993,251.924,337.58,222.491,337.58Zm195.38,0c-28.971,0-52.843-26.587-52.843-59.239S388.437,219.1,417.871,219.1c29.667,0,53.307,26.82,52.844,59.239C470.715,310.993,447.538,337.58,417.871,337.58Z"/>
      </svg>
    </a>
</div>
<div className="xl:h-32 xl:w-32 bg-BA_grey rounded-[33px] mobile:rounded-[20px] flex z-10 lg:h-24 lg:w-24 mobile:h-16 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
  <a href='https://medium.com/@boysadventure' target="blank_" className='flex flex-row w-full'>
    <svg viewBox="0 0 1043.63 592.71" className="xl:h-12 xl:w-12 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white"><g data-name="Layer 2"><g data-name="Layer 1"><path d="M588.67 296.36c0 163.67-131.78 296.35-294.33 296.35S0 460 0 296.36 131.78 0 294.34 0s294.33 132.69 294.33 296.36M911.56 296.36c0 154.06-65.89 279-147.17 279s-147.17-124.94-147.17-279 65.88-279 147.16-279 147.17 124.9 147.17 279M1043.63 296.36c0 138-23.17 249.94-51.76 249.94s-51.75-111.91-51.75-249.94 23.17-249.94 51.75-249.94 51.76 111.9 51.76 249.94"></path></g></g></svg>
  </a> 
</div>
 */
//       <a href="https://twitter.com/BoysAdvnture" target="blank_" className='flex flex-row justify-center w-full text-2xl tracking-[1px]'>MINT</a>

  return (
    <>
      <div className="xl:h-32 xl:w-32 bg-BA_grey rounded-[33px] mobile:rounded-[20px] flex z-10 lg:h-24 lg:w-24 mobile:h-16 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
        <a href='https://twitter.com/BoysAdvnture' target="blank_" className='flex flex-row w-full'>
          <svg viewBox="0 0 20 20" aria-hidden="true" class="xl:h-12 xl:w-12 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white"><path d="M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0 0 20 3.92a8.19 8.19 0 0 1-2.357.646 4.118 4.118 0 0 0 1.804-2.27 8.224 8.224 0 0 1-2.605.996 4.107 4.107 0 0 0-6.993 3.743 11.65 11.65 0 0 1-8.457-4.287 4.106 4.106 0 0 0 1.27 5.477A4.073 4.073 0 0 1 .8 7.713v.052a4.105 4.105 0 0 0 3.292 4.022 4.095 4.095 0 0 1-1.853.07 4.108 4.108 0 0 0 3.834 2.85A8.233 8.233 0 0 1 0 16.407a11.615 11.615 0 0 0 6.29 1.84"></path></svg>
        </a>
      </div>
      <div className="xl:h-64 xl:w-32 bg-BA_blue rounded-[33px] mobile:rounded-[20px] flex flex-col justify-center text-BA_white font-boldstrom z-10 lg:h-56 lg:w-24 mobile:h-48 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
        <a href="https://twitter.com/BoysAdvnture" target="blank_" className='flex flex-col justify-center'>
          <img src={'/assets/Aptos_Logo.svg'} alt="Aptos logo" className="h-[58%] fill-BA_white"/>
        </a>
      </div>
      <div className="xl:h-32 xl:w-32 bg-BA_grey rounded-[33px] mobile:rounded-[20px] flex z-10 lg:h-24 lg:w-24 mobile:h-16 mobile:w-16 transition ease-in-out delay-[50ms] hover:-translate-y-1 hover:scale-110 duration-300">
        <a href='https://discord.com/invite/wfZ8xMkF2h' target="blank_" className='flex flex-row w-full'>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" className='xl:h-14 xl:w-14 m-auto lg:h-8 lg:w-8 mobile:h-6 mobile:w-6 fill-BA_white'>
            <path d="M524.531,69.836a1.5,1.5,0,0,0-.764-.7A485.065,485.065,0,0,0,404.081,32.03a1.816,1.816,0,0,0-1.923.91,337.461,337.461,0,0,0-14.9,30.6,447.848,447.848,0,0,0-134.426,0,309.541,309.541,0,0,0-15.135-30.6,1.89,1.89,0,0,0-1.924-.91A483.689,483.689,0,0,0,116.085,69.137a1.712,1.712,0,0,0-.788.676C39.068,183.651,18.186,294.69,28.43,404.354a2.016,2.016,0,0,0,.765,1.375A487.666,487.666,0,0,0,176.02,479.918a1.9,1.9,0,0,0,2.063-.676A348.2,348.2,0,0,0,208.12,430.4a1.86,1.86,0,0,0-1.019-2.588,321.173,321.173,0,0,1-45.868-21.853,1.885,1.885,0,0,1-.185-3.126c3.082-2.309,6.166-4.711,9.109-7.137a1.819,1.819,0,0,1,1.9-.256c96.229,43.917,200.41,43.917,295.5,0a1.812,1.812,0,0,1,1.924.233c2.944,2.426,6.027,4.851,9.132,7.16a1.884,1.884,0,0,1-.162,3.126,301.407,301.407,0,0,1-45.89,21.83,1.875,1.875,0,0,0-1,2.611,391.055,391.055,0,0,0,30.014,48.815,1.864,1.864,0,0,0,2.063.7A486.048,486.048,0,0,0,610.7,405.729a1.882,1.882,0,0,0,.765-1.352C623.729,277.594,590.933,167.465,524.531,69.836ZM222.491,337.58c-28.972,0-52.844-26.587-52.844-59.239S193.056,219.1,222.491,219.1c29.665,0,53.306,26.82,52.843,59.239C275.334,310.993,251.924,337.58,222.491,337.58Zm195.38,0c-28.971,0-52.843-26.587-52.843-59.239S388.437,219.1,417.871,219.1c29.667,0,53.307,26.82,52.844,59.239C470.715,310.993,447.538,337.58,417.871,337.58Z"/>
          </svg>
        </a> 
      </div> 
    </>
  )
}

