import React from 'react';
import Logo from './Logo';

export default function Aptos() {
    return (
        <section id="aptos" class="snap-start h-[100vh] w-max">
            <div className="h-full w-screen bg-center bg-cover bg-no-repeat flex flex-col justify-center" style={{ backgroundImage: "url(/assets/Aptos_waves.png)"}}>
                <div className="xl:h-32 lg:h-32 mobile:h-28 mx-auto">
                    <Logo/>
                </div>
            </div>
        </section>
  )
}
