import React from 'react'

export default function Slider() {
  return (
    <div className="w-2/4 xl:m-auto lg:m-auto items-center h-full flex justify-center xl:ml-0 lg:ml-0 mobile:ml-[15%] xl:pt-0 lg:pt-0 mobile:pt-8">
      <div className='carousel relative w-full flex justify-center xl:max-w-[500px] lg:max-w-[500px] xl:-mt-[45%] lg:-mt-[55%] mobile:-mt-[60%]'>
        <div className='carousel__item flex items-center absolute xl:w-[62%] lg:w-[50%] mobile:w-[60%] opacity-0 will-change-[opacity,transform] px-0 py-3'>
          <img src="https://i.ibb.co/JzWKcwd/NFT1.jpg" alt="" className="xl:h-80 lg:h-64 mobile:h-32 xl:rounded-[90px] lg:rounded-[90px] mobile:rounded-[50px] m-auto"/>
        </div>
        <div className='carousel__item flex items-center absolute xl:w-[65%] lg:w-[50%] mobile:w-[60%] opacity-0 will-change-[opacity,transform] px-0 py-3'>
          <img src="https://i.ibb.co/9tYk5Wq/NFT2.jpg" alt="" className="xl:h-80 lg:h-64 mobile:h-32 xl:rounded-[90px] lg:rounded-[90px] mobile:rounded-[50px] m-auto"/>
        </div>
        <div className='carousel__item flex items-center absolute xl:w-[65%] lg:w-[50%] mobile:w-[60%] opacity-0 will-change-[opacity,transform] px-0 py-3'>
          <img src="https://i.ibb.co/JphLW7j/NFT3.jpg" alt="" className="xl:h-80 lg:h-64 mobile:h-32 xl:rounded-[90px] lg:rounded-[90px] mobile:rounded-[50px] m-auto"/>
        </div>
        <div className='carousel__item flex items-center absolute xl:w-[65%] lg:w-[50%] mobile:w-[60%] opacity-0 will-change-[opacity,transform] px-0 py-3'>
          <img src="https://i.ibb.co/6FBWfMR/NFT4.jpg" alt="" className="xl:h-80 lg:h-64 mobile:h-32 xl:rounded-[90px] lg:rounded-[90px] mobile:rounded-[50px] m-auto"/>
        </div>
        <div className='carousel__item flex items-center absolute xl:w-[65%] lg:w-[50%] mobile:w-[60%] opacity-0 will-change-[opacity,transform] px-0 py-3'>
          <img src="https://i.ibb.co/WpRwndf/NFT5.png" alt="" className="xl:h-80 lg:h-64 mobile:h-32 xl:rounded-[90px] lg:rounded-[90px] mobile:rounded-[50px] m-auto"/>
        </div>
      </div>
    </div>
  )
}
